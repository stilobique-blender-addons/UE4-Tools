import bpy

# TODO UI label to expensive


class CollectionToExport(bpy.types.Operator):
    """Operator to add or remove a collection to export UE4"""
    bl_idname = "collection.export"
    bl_label = "Export to UE4"

    def execute(self, context):
        # Check if this collection is setup to export :
        #   If not, add a property
        #   Else, remove this property
        export = context.collection.Export.ToExport
        name = context.collection.name
        print('Export ', context.collection.Export.ToExport)

        if export:
            bpy.data.collections[name].Export.ToExport = False

        else:
            bpy.data.collections[name].Export.ToExport = True

        return {'FINISHED'}
