import bpy

from bpy.types import Operator


class ManageAssets(bpy.types.Operator):
    """Insert a simple description"""
    bl_idname = "object.manage_assets"
    bl_label = "Make the selected object as an Assets"

    def execute(self, context):
        slt_obj = context.active_object
        obj_name = slt_obj.name
        obj_data = bpy.data.objects
        parentname = 'assets_element'

        if bpy.data.objects.get(parentname) is None:
            bpy.ops.object.add(type='EMPTY')
            ob = context.active_object
            ob.name = parentname

            bpy.ops.object.select_all(action='DESELECT')

        # Make the empty as parent
        context.scene.objects.active = obj_data[obj_name]
        obj_data[obj_name].parent = obj_data[parentname]

        return {'FINISHED'}


class GenerateAsset(Operator):
    """Transform the selected object on a asset to be export"""
    bl_idname = "asset.generate"
    bl_label = "Generate an asset to export it"

    @classmethod
    def poll(cls, context):
        return context.object.type == 'MESH'

    def execute(self, context):
        print('Generate an asset')

        asset_name = 'SM_Object'
        if asset_name not in bpy.data.groups[-1]:
            bpy.data.groups.new(asset_name)

            for obj in context.selected_objects:
                obj.select = True
                bpy.ops.object.group_link(group=asset_name)

        # Generate asset
        # Add groups "SM_XXX"
        # Add a properties with this groups

        return {'FINISHED'}


def register():
    bpy.utils.register_class(ManageAssets)
    bpy.utils.register_class(GenerateAsset)


def unregister():
    bpy.utils.unregister_class(ManageAssets)
    bpy.utils.unregister_class(GenerateAsset)
