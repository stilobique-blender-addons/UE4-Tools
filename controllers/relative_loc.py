import bpy
import mathutils
import re

from .. import pyperclip
from bpy.props import BoolProperty


class GetRelativeLocation(bpy.types.Operator):
    """Paste on the buffer, the relative location"""
    bl_idname = "object.relat_loc"
    bl_label = "Get the relative location"

    copy: BoolProperty(default=True)

    def execute(self, context):
        obj_loc = context.active_object.location
        print('Copy value : ', self.copy)

        if self.copy:
            # Copy location
            if context.active_object.parent:
                obj_loc_parent = context.active_object.parent.location

            else:
                obj_loc_parent = [1, 1, 1]
            obj_loc_x = round(obj_loc[0]*100, 6)
            obj_loc_y = round(obj_loc[1]*-100, 6)
            obj_loc_z = round(obj_loc[2]*100, 6)
            string_location = '(X={},Y={},Z={})'.format(obj_loc_x, obj_loc_y,
                                                        obj_loc_z)
            self.report({'INFO'}, "{}".format(string_location))
            pyperclip.copy(str(string_location))

        else:
            # Paste a location
            string_location = pyperclip.paste()
            new_loc = re.findall(r'\d+', str(string_location))
            x_loc = new_loc[0] + '.' + new_loc[1]
            y_loc = new_loc[2] + '.' + new_loc[3]
            z_loc = new_loc[4] + '.' + new_loc[5]
            context.active_object.location = mathutils.Vector((float(x_loc)/100,
                                                               float(y_loc)/-100,
                                                               float(z_loc)/100))

        return {'FINISHED'}
