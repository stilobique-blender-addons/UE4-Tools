import bpy


def check_layers():
    """
    This function check if one layer has activate.
    He return a Boolean with the value False if one layer is not activate.
    :return: state, a boolean with False if one layer is hidden.
    """
    list_layers = bpy.context.scene.layers
    state = True

    for layer in list_layers:
        if layer is False:
            state = False
            break

    return state


def group_hide(name_group):
    """
    Look if the group is restrict render
    :param name_group: group name, a simple string
    :return: Boolean type
    """
    group = bpy.data.groups[name_group].objects
    for mesh in group:
        if mesh.hide_render:
            return True

    return False


def group_convert(context, convert, object_name, object_group):
    """
    This function convert a group instance to static mesh, when it's done he
    regenerate the group instance
    :param context: Blender context
    :param convert: Boolean to convert or transform
    :param object_name: Object name (bpy.data.objects['name'].name)
    :param object_group: Group name (bpy.data.groups['name'].name)
    :return:
    """
    print("Convert {0}".format(object_name))
    if convert:
        print("Group to Mesh")
        bpy.ops.object.duplicates_make_real()
        print("Object converted {0}".format(context.selected_objects))

        for mesh in context.selected_objects:
            print("Export object {0}".format(mesh.name))
            if mesh.type == 'MESH':
                mesh.select = True
            else:
                mesh.select = False

    else:
        print("Regenerate group instance")
        bpy.data.objects[object_name].dupli_group = bpy.data.groups[
            object_group]


class ExportScene(bpy.types.Operator):
    """Export all stadium element"""
    bl_idname = "object.exports_batch"
    bl_label = "Export all objects"

    @classmethod
    def poll(cls, context):
        return context.area.spaces.active.local_view is None

    def execute(self, context):
        scn = context.scene
        list_groups = bpy.data.groups
        count = 0
        frame_save = context.scene.frame_current

        context.scene.frame_set(0)
        if check_layers() is False:
            bpy.ops.view3d.layers(nr=0, extend=False)

        for grp in list_groups:
            print("Export Start --------------------")
            print("Export asset {0}".format(grp.name))
            file_name = scn.conf_path + grp.name + '.fbx'
            for mesh in grp.objects:
                if mesh.type == 'MESH':
                    print("Export object {0}".format(mesh.name))
                    mesh.select = True

                # elif mesh.type == 'EMPTY' and mesh.dupli_group is not None:
                #     print("Export group {0}".format(mesh.name))
                #     group_convert(context=context,
                #                   convert=True,
                #                   object_name=mesh.name,
                #                   object_group=mesh.dupli_group.name)

            if bpy.context.selected_objects and not group_hide(grp.name):
                """The exported is based on Unreal Engine 4 setup"""
                bpy.ops.export_scene.fbx(filepath=file_name,
                                         version='BIN7400',
                                         axis_forward='-X',
                                         axis_up='Z',
                                         use_selection=True,
                                         bake_space_transform=True,
                                         object_types={'MESH'},
                                         use_mesh_modifiers=True,
                                         mesh_smooth_type='FACE',
                                         use_mesh_edges=True,
                                         # use_tspace=True,
                                         # bake_anim=False
                                         # batch_mode='GROUP',
                                         # use_batch_own_dir=False
                                         )
                count += 1
                print("Export Finish -------------------")

            bpy.ops.object.select_all(action='DESELECT')

        bpy.ops.view3d.layers(nr=0, extend=False)
        context.scene.frame_set(frame_save)

        self.report({'INFO'}, "{} object(s) has exports.".format(count))

        return {'FINISHED'}
