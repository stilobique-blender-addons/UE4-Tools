import bpy
import csv


def dataspline(object_name, curve_name, path_out):
    data = [
        ['', 'XPos', 'YPos', 'ZPos', 'XInTangent', 'YInTangent', 'ZInTangent',
         'XOutTangent', 'YOutTangent', 'ZOutTangent'],
    ]

    if path_out is None:
        path_out = r'C:/'
    outputfile = str(path_out) + str(object_name) + '.csv'
    print('Path info : ', path_out)
    csvfile = csv.writer(open(outputfile, 'w'),
                         delimiter=',')

    # All point spline
    spline = bpy.data.curves[curve_name].splines[0].bezier_points
    scale = 100

    for key in range(len(spline)):
        x_value = round(spline[key].co[0], 6)
        y_value = round(spline[key].co[1], 6) * -1
        z_value = round(spline[key].co[2], 6)

        xt_in = round(spline[key].handle_left[0], 6)
        yt_in = round(spline[key].handle_left[1], 6) * -1
        zt_in = round(spline[key].handle_left[2], 6)

        xt_out = round(spline[key].handle_right[0], 6)
        yt_out = round(spline[key].handle_right[1], 6) * -1
        zt_out = round(spline[key].handle_right[2], 6)

        print('vert' + str(key))
        data.append(['vert' + str(key),
                     x_value * scale, y_value * scale, z_value * scale,
                     xt_in * scale, yt_in * scale, zt_in * scale,
                     xt_out * scale, yt_out * scale, zt_out * scale
                    ])

    for value in data:
        csvfile.writerow(value)


class SplineCSV(bpy.types.Operator):
    """Create a database for a spline object"""
    bl_idname = "object.spline_csv"
    bl_label = "Database Spline CSV"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.active_object is not None and \
               context.active_object.type == 'CURVE'

    def execute(self, context):
        obj_name = context.object.name
        curve_name = context.object.data.name
        path_export = context.scene.ue_path.export_folder
        dataspline(obj_name, curve_name, path_export)

        self.report({'INFO'}, "Curve {0} export.".format(obj_name))

        return {'FINISHED'}


def register():
    bpy.utils.register_class(SplineCSV)


def unregister():
    bpy.utils.unregister_class(SplineCSV)
