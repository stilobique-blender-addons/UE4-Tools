import bpy
import sys
import importlib

from .models import Paths, CollectionsProperty
from .views import check, PanelViewport, pie, various
from .views.Collections import export_ue4, collection_filter_export
from .controllers import Collections, data_buffer, ExportProps, SplineDatas, relative_loc

from bpy.utils import register_class, unregister_class
from bpy.props import PointerProperty

bl_info = {
    "name": "UE4 Tools",
    "description": "Various tools to make more easy the Game Art",
    "author": "stilobique",
    "version": (0, 3, 0),
    "blender": (2, 80, 0),
    "location": "Viewport Sidebar > GA Tools",
    "warning": "Beta",  # used for warning icon and text in addons panel
    "wiki_url": "https://gitlab.com/stilobique-blender-addons/UE4-Tools/wikis/home",
    "tracker_url": "https://gitlab.com/stilobique-blender-addons/UE4-Tools/issues",
    "support": "COMMUNITY",
    "category": "Object"
}


classes = [
    # Models,
    CollectionsProperty.CollectionExportSettings,
    Paths.PathExport,
    # Views,
    # check.MESH_PT_CheckerObjects (need more refactoring groups/collection)
    PanelViewport.MESH_PT_ExportUE4,
    # Controllers,
    Collections.CollectionToExport,
    data_buffer.DataBuffer,
    ExportProps.ExportScene,
    SplineDatas.SplineCSV,
    relative_loc.GetRelativeLocation,
]


def register():
    for cls in classes:
        register_class(cls)

    # Outliner menu edit
    bpy.types.OUTLINER_MT_context.prepend(export_ue4)
    bpy.types.OUTLINER_PT_filter.prepend(collection_filter_export)
    # Declare all blender variable
    bpy.types.Scene.ue_path = PointerProperty(type=Paths.PathExport)
    bpy.types.Collection.Export = PointerProperty(type=CollectionsProperty.CollectionExportSettings)


def unregister():
    for cls in reversed(classes):
        unregister_class(cls)
