import bpy
import mathutils


def validate_transform(obj, layout):
    icon_location = icon_rotation = icon_scale = "ERROR"
    zero_loc = mathutils.Vector((0, 0, 0))
    zero_rot = mathutils.Euler((0, 0, 0), 'XYZ')
    zero_scale = mathutils.Vector((1, 1, 1))
    if obj.location == zero_loc:
        icon_location = "FILE_TICK"
    if obj.rotation_euler == zero_rot:
        icon_rotation = "FILE_TICK"
    if obj.scale == zero_scale:
        icon_scale = "FILE_TICK"

    row = layout.split(align=True)
    ops = 'object.transform_apply'
    location = row.operator(ops, text="L", icon=icon_location)
    location.location = True
    location.rotation = location.scale = False
    rotation = row.operator(ops, text="R", icon=icon_rotation)
    rotation.location = rotation.scale = False
    rotation.rotation = True
    scale = row.operator(ops, text="S", icon=icon_scale)
    scale.location = scale.rotation = False
    scale.scale = True


def validate_lightmap(obj, layout):
    text = "No Lightmap UV"
    icon = "ERROR"
    uvs = obj.data.uv_layers
    if len(uvs) >= 2:
        text = "UV Bad name"
        if uvs[1] and uvs[1].name == 'UV Lightmap':
            text = "Lightmap"
            icon = "FILE_TICK"

    layout.label(text=text, icon=icon)


def validate_material(obj, layout):
    text = "No Material"
    icon = "ERROR"
    if len(obj.material_slots) >= 1:
        if obj.material_slots[0]:
            text = "Material(s)"
            icon = "FILE_TICK"
    layout.label(text=text, icon=icon)


def validate_smoothing(obj, layout):
    text = "No Smoothing Group"
    icon = "ERROR"
    if len(obj.data.polygons) >= 1:
        if obj.data.polygons[0].use_smooth:
            text = "Smoothing Groups"
            icon = "FILE_TICK"
    layout.label(text=text, icon=icon)


def validate_polygon(obj, layout):
    text = "Only Quad and Tris"
    icon = "FILE_TICK"
    count = 0
    for p in obj.data.polygons:
        faces = p.loop_total
        if not (faces == 3 or faces == 4):
            count += 1

    if count:
        text = "Bad Wireframe, Ngons"
        icon = "ERROR"

    layout.label(text=text, icon=icon)


def count_tris_group(list_obj):
    base_triangles = 0
    total_triangles = 0
    # bpy.data.object[]
    for obj in list_obj:
        tris_obj = 0
        if obj.type == 'MESH':
            for face in obj.data.polygons:
                vertices = face.vertices
                triangles = len(vertices) - 2
                tris_obj += triangles
                base_triangles += triangles
        if obj.modifiers:
            for md in obj.modifiers:
                if md.type == 'SUBSURF':
                    # TODO Bad count
                    level = md.render_levels
                    total_triangles = (tris_obj * level) + base_triangles
                if md.type == 'ARRAY':
                    total_triangles = (tris_obj * md.count) + base_triangles

    return base_triangles, total_triangles


# -----------------------------------------------------------------------------
# Checker Object panel
# -----------------------------------------------------------------------------
class MESH_PT_CheckerObjects(bpy.types.Panel):
    bl_idname = 'MESH_PT_CheckerObjects'
    bl_label = "Validate"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "GA Tools"
    bl_context = "objectmode"

    @classmethod
    def poll(cls, context):
        return context.object is not None

    def draw(self, context):
        layout = self.layout
        obj = context.active_object

        if context.active_object is not None:
            if obj.type == 'MESH':
                if obj.users_collection:
                    for grp in obj.users_group:
                        groups = bpy.data.groups
                        layout.prop(groups[grp.name], 'name', text="",
                                    icon="OUTLINER_OB_GROUP_INSTANCE")

                    validate_transform(obj, layout)
                    validate_lightmap(obj, layout)
                    validate_material(obj, layout)
                    validate_smoothing(obj, layout)
                    validate_polygon(obj, layout)
                    group = obj.users_group[0].objects
                    layout.label("Total Tris : {0}".format(count_tris_group(
                        group
                        )))

                else:
                    layout.label(text="No asset")

            else:
                layout.label("This object can't be export")
