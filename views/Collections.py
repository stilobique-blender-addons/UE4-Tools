import bpy

from ..controllers.Collections import CollectionToExport


def export_ue4(self, context):
    # TODO add only with on a collection context
    layout = self.layout
    layout.separator()
    layout.operator(CollectionToExport.bl_idname)


def collection_filter_export(self, context):
    # TODO add to a filter function (possible ?)
    layout = self.layout
    layout.separator()
    layout.label(text='New Filter -WIP')
