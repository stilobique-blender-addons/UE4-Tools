import bpy

from bpy.props import StringProperty


class PathExport(bpy.types.PropertyGroup):
    export_folder: StringProperty(
        name='Folder Export',
        description="Choose where you want export your file",
        default='',
        subtype='DIR_PATH',
    )
